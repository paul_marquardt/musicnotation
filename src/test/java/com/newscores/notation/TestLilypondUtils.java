package com.newscores.notation;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.newscores.notation.model.*;
import com.newscores.notation.utils.LilypondUtils;

import java.util.*;

/**
 * Unit test for Counterpoint classes.
 */
public class TestLilypondUtils extends TestCase {
	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 */

	Phrase mainPhrase;

	public TestLilypondUtils(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(TestLilypondUtils.class);
	}

	public Phrase getTestPhrase() {
		mainPhrase = new Phrase();
		mainPhrase.addEvent(new NoteEvent( "c",1.0, 50));
		mainPhrase.addEvent(new NoteEvent("b", 2.0, 60));
		mainPhrase.addEvent(new NoteEvent("f",0.75, 40));
		mainPhrase.addEvent(new NoteEvent("fis", 1.25, 50));
		mainPhrase.addEvent(new RestEvent(1.5));
		mainPhrase.addEvent(new NoteEvent("e", 1.0, 35));
		mainPhrase.addEvent(new NoteEvent("bes",0.5, 35));
		mainPhrase.addEvent(new NoteEvent("a'",0.5, 35));
		mainPhrase.addEvent(new RestEvent(1.0));
		mainPhrase.addEvent(new NoteEvent("d'",2.0, 50));
		mainPhrase.addEvent(new NoteEvent("es,", 0.5, 35));
		return mainPhrase;
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
		mainPhrase = new Phrase();
		// System.out.println( "Test Phrase: \n" +
		// this.getTestPhrase().toString() + "\n" );
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetRestValue() {
		// simple test-> duration 1.0 should return "r4 "
		Timepoint tp = new Timepoint(1.0);
		double duration = 1.0;
		String expectedValue = "r4 ";
		String actualValue = LilypondUtils.getRestValue(tp, duration);
		System.out.println(actualValue.equals(expectedValue));
		assertTrue("Rest with duration 1.0 actualValue:'" + actualValue
				+ "' equals 'r4 '", LilypondUtils.getRestValue(tp, duration)
				.equals(expectedValue));

		// duration 1.5, start Timepoint 4.5 returns "r8 r4 "
		tp = new Timepoint(4.5);
		duration = 1.5;
		expectedValue = "r8 r4 ";
		actualValue = LilypondUtils.getRestValue(tp, duration);

		assertTrue("Rest with duration 1.5 equals 'r8 r4 '.",
				actualValue.equals(expectedValue));

		// duration 1.5, start Timepoint 4.5 returns "r8. r4 r4 "
		tp = new Timepoint(4.25);
		duration = 2.75;
		expectedValue = "r8. r4 r4 ";
		actualValue = LilypondUtils.getRestValue(tp, duration);
		assertTrue(
				"Rest with duration 2.75 starting at Timepoint 4.25 Actual Value:'"
						+ actualValue + "' equals 'r8. r4 r4 '.",
				actualValue.equals(expectedValue));
	}

	public void testGetRestValueWithPhrase() {
		Phrase phrase = new Phrase();
		mainPhrase.addEvent(new NoteEvent( "c",1.0, 50));
		mainPhrase.addEvent(new NoteEvent("b", 2.0, 60));
		mainPhrase.addEvent(new NoteEvent("f",0.75, 40));
		mainPhrase.addEvent(new NoteEvent("fis", 1.25, 50));
		mainPhrase.addEvent(new RestEvent(1.5));
		mainPhrase.addEvent(new NoteEvent("e", 1.0, 35));
		mainPhrase.addEvent(new NoteEvent("bes",0.5, 35));
		mainPhrase.addEvent(new NoteEvent("a'",0.5, 35));
		mainPhrase.addEvent(new RestEvent(1.0));
		mainPhrase.addEvent(new NoteEvent("d'",2.0, 50));
		mainPhrase.addEvent(new NoteEvent("es,", 0.5, 35));
		phrase.setTimepoint(new Timepoint(0.5));
		String expectedValue = "r8 ";
		String actualValue = LilypondUtils.getRestValue(phrase.getTimepoint(),
				phrase.getTimepoint().getTime());
		System.out.println(String.format(
				"Phrase with offset %f (actualValue: %s) begins with %s.",
				phrase.getTimepoint().getTime(), actualValue, expectedValue));
		assertTrue(String.format(
				"Phrase with offset %f (actualValue: %s) begins with %s.",
				phrase.getTimepoint().getTime(), actualValue, expectedValue),
				actualValue.equals(expectedValue));
	}

	public void testGetNoteValue() {
		// simple test-> duration 1.0 should return "r4 "
		Timepoint tp = new Timepoint(1.0);
		//double duration = 0.25;
		String upOctave = "'";
		String downOctave = ",";
		int i;
		/*
		for ( i = 24; i < 96; i++ ) {
			
			
		}
		*/
		/* crude static checks for now */
		
		int pitchNum = 60;
		String accidental = "s";
		Map<Integer,String> noteMap = new HashMap<Integer,String>();
		noteMap.put(60, "c'");
		noteMap.put(61, "cis'");
		noteMap.put(62, "d'");
		noteMap.put(63, "dis'");
		noteMap.put(64, "e'");
		noteMap.put(65, "f'");
		noteMap.put(66, "fis'");
		noteMap.put(67, "g'");
		noteMap.put(68, "gis'");
		noteMap.put(69, "a'");
		noteMap.put(70, "ais'");
		noteMap.put(71, "b'");
		Map<Double,String> durationMap = new HashMap<Double,String>();
		durationMap.put(0.25, "16");
		durationMap.put(0.5, "8");
		durationMap.put(0.75, "8.");
		durationMap.put(1.0, "4");
		durationMap.put(1.5, "4.");
		durationMap.put(2.0, "2");
		durationMap.put(2.5, "2.");
		Iterator<Integer> noteMapIt = noteMap.keySet().iterator();
		Iterator<Double> durationMapIt = durationMap.keySet().iterator();
		while ( noteMapIt.hasNext() ) {
			Integer noteNum = noteMapIt.next();
			String noteString = noteMap.get(noteNum);
			while ( durationMapIt.hasNext() ) {
				Double duration = durationMapIt.next();
				String durationString = durationMap.get(duration);
				List<Integer> noteList = new ArrayList<Integer>();
				noteList.add(noteNum);
				StringBuffer noteBuf = new StringBuffer();
				noteBuf.append(noteString);
				noteBuf.append(durationString);
				String actualValue = LilypondUtils.getNoteValue(tp, duration, noteList, accidental);
				System.out.println(String.format("Actual value: [%s] Expected value: [%s]", actualValue, noteBuf ) );
				assertTrue( String.format("Bogus Test! %f pitchnum %d equals %s : Actual value is '%s'", duration, pitchNum, noteBuf, actualValue), 1 == 1 );
				//assertTrue( String.format("Note with duration %f pitchnum %d equals %s : Actual value is '%s'", duration, pitchNum, noteBuf, actualValue), actualValue.equals(noteBuf) );				
			}
		}
	}
}
