package com.newscores.notation;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.newscores.notation.model.*;
import com.newscores.notation.decorators.LilypondAbstractPhraseDecorator;

import java.util.*;

/**
 * Unit test for Counterpoint classes.
 */
public class TestLilypondAbstractPhraseDecorator extends TestCase {
	/**
	 * Create the test case
	 * 
	 * @param testName
	 *            name of the test case
	 */

	List<Phrase> mainPhraseList;

	public TestLilypondAbstractPhraseDecorator(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(TestLilypondAbstractPhraseDecorator.class);
	}

	public List<Phrase> getTestPhrase() {
		mainPhraseList = new ArrayList<Phrase>();
		Phrase phr = new Phrase();
		
		phr.addEvent(new NoteEvent("c",1.0, 50));
		phr.addEvent(new NoteEvent("b",2.0, 60));
		phr.addEvent(new NoteEvent("f", 0.75, 40));
		phr.addEvent(new NoteEvent("e", 1.25, 50));
		mainPhraseList.add( phr );
		
		phr = new Phrase();
		phr.addEvent(new RestEvent(1.5));
		phr.addEvent(new NoteEvent("e", 1.0, 35));
		phr.addEvent(new NoteEvent("bes",0.5, 35));
		phr.addEvent(new NoteEvent("a'",0.5, 35));
		phr.addEvent(new RestEvent(1.0));
		mainPhraseList.add( phr );
		
		phr = new Phrase();
		phr.addEvent(new RestEvent(1.0));
		phr.addEvent(new NoteEvent("g",0.75, 35));
		phr.addEvent(new NoteEvent("es'", 0.5, 35));
		phr.addEvent(new NoteEvent("c'",0.5, 35));
		mainPhraseList.add( phr );
		return mainPhraseList;
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
		// System.out.println( "Test Phrase: \n" +
		// this.getTestPhrase().toString() + "\n" );
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	public void testLilypondAbstractPhraseDecorator() {
		LilypondAbstractPhraseDecorator lpd = new LilypondAbstractPhraseDecorator( this.getTestPhrase() );
		String phraseChars = "\nc'4";
		System.out.println("LilypondDecorator output:\n" + lpd.output());
		//assertTrue("LilypondPhraseDecorator contains 'c4'", lpd.output()
		//		.contains(phraseChars));
	}

}
