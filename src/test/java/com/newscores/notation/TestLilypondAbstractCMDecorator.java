package com.newscores.notation;

import com.newscores.notation.decorators.LilypondAbstractCMDecorator;
import com.newscores.notation.model.NoteEvent;
import com.newscores.notation.model.Phrase;
import com.newscores.notation.model.RestEvent;
import com.newscores.setTheory.CompositionMatrix;
import com.newscores.setTheory.CMSegment;
import com.newscores.setTheory.PitchClassSequence;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for Counterpoint classes.
 */
public class TestLilypondAbstractCMDecorator extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */

	List<Phrase> mainPhraseList;

	public TestLilypondAbstractCMDecorator(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(TestLilypondAbstractCMDecorator.class);
	}


	@Override
	public void setUp() throws Exception {
		super.setUp();
		// System.out.println( "Test Phrase: \n" +
		// this.getTestPhrase().toString() + "\n" );
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	public void testLilypondAbstractPhraseDecorator() {
		LilypondAbstractCMDecorator lpd = new LilypondAbstractCMDecorator( new CompositionMatrix(), "testCM" );
		String phraseChars = "\nc'4";
		System.out.println("LilypondDecorator output:\n" + lpd.output());
		//assertTrue("LilypondPhraseDecorator contains 'c4'", lpd.output()
		//		.contains(phraseChars));
	}

}
