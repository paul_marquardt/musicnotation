package com.newscores.notation;

import com.newscores.notation.interfaces.ObjectDecoratorInterface;
import com.newscores.notation.model.Phrase;

public class MusicXMLPhraseDecorator implements ObjectDecoratorInterface {

	// private Phrase phraseToDecorate;
	public static final String TREBLE_CLEF = "\\clef treble\n";
	public static final String BASS_CLEF = "\\clef bass\n";
	public static final String DEFAULT_CLEF = TREBLE_CLEF;
	public static final String DEFAULT_ACCIDENTAL = "s";
	private String musicXMLOutput;
	private StringBuffer outputBuffer;
	private Phrase phraseToDecorate;

	public MusicXMLPhraseDecorator(Phrase inPhrase) {
		// this.phraseToDecorate = inPhrase;
		this.musicXMLOutput = new String();
		this.outputBuffer = new StringBuffer();
		//this.writeStaff(inPhrase);
	}

	public String output() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
