package com.newscores.notation;

import com.newscores.notation.model.Phrase;
import com.newscores.notation.decorators.LilypondPhraseDecorator;

public class ObjectFactories {
	
	public static LilypondPhraseDecorator getLilypondPhraseDecorator(Phrase inPhrase) {
		return new LilypondPhraseDecorator(inPhrase);
	}
}
