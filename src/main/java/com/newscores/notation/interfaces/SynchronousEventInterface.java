package com.newscores.notation.interfaces;

import com.newscores.notation.model.Timepoint;

public interface SynchronousEventInterface extends EventInterface {

	Timepoint getTimepoint();

	void setTimepoint(Timepoint timepoint);

}
