package com.newscores.notation.interfaces;

public interface ObjectDecoratorInterface {

	public String output();
}
