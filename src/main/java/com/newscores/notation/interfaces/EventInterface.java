package com.newscores.notation.interfaces;

public interface EventInterface {

    double getDuration();

    void setDuration(double duration);
}