package com.newscores.notation.interfaces;

import com.newscores.notation.model.BaseEvent;
import com.newscores.notation.model.NoteEvent;
import com.newscores.notation.model.RestEvent;
import com.newscores.notation.model.Timepoint;

import java.util.List;

public interface PhraseInterface {

	public NoteEvent getNoteEventAtTimepoint(Timepoint inTimepoint);

	public List<SynchronousEventInterface> getSynchronousEvents();

	public boolean addEvent(RestEvent eventToAdd);

	public boolean addEvent(NoteEvent eventToAdd);

	public boolean removeEventAtTimepoint(Timepoint timepoint);

	public Timepoint getTimepoint();

	public void setTimepoint(Timepoint timepoint);

	public double getDuration();

	// getEndTimepoint returns a Timepoint representing the time when the last
	// NoteEvent of the phrase ends.
	public Timepoint getEndTimepoint();

	public PhraseInterface transpose(int transposition);

	public PhraseInterface inversion();

	public PhraseInterface retrograde();

	public PhraseInterface offset(double amountToOffset);

	public List<BaseEvent> getEvents();

	public void setEvents(List<BaseEvent> events);

	public boolean phraseEquals(PhraseInterface anotherPhrase);

	public void setTimepointIncrement(double increment);

	public double getTimepointIncrement();

	public String toString();

	public String asTimeline();

	public String getDescription();

	public void setDescription(String phraseDescription);

}
