package com.newscores.notation.interfaces;

public interface NoteInterface {

	public int getPitch();

	public int setPitch(int inPitch);
}
