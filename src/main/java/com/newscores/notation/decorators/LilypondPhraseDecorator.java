package com.newscores.notation.decorators;

import java.util.Iterator;
import java.util.List;

import com.newscores.notation.model.Phrase;
import com.newscores.notation.interfaces.SynchronousEventInterface;
import com.newscores.notation.model.SynchronousNoteEvent;
import com.newscores.notation.model.SynchronousRestEvent;
import com.newscores.notation.model.Timepoint;
import com.newscores.notation.utils.LilypondUtils;
import com.newscores.notation.interfaces.ObjectDecoratorInterface;

public class LilypondPhraseDecorator implements ObjectDecoratorInterface {

	// private Phrase phraseToDecorate;
	public static final String TREBLE_CLEF = "\\clef treble\n";
	public static final String BASS_CLEF = "\\clef bass\n";
	public static final String DEFAULT_CLEF = TREBLE_CLEF;
	public static final String DEFAULT_ACCIDENTAL = "s";
	private String lilypondOutput;
	private StringBuffer outputBuffer;
	private Phrase phraseToDecorate;

	public LilypondPhraseDecorator(Phrase inPhrase) {
		// this.phraseToDecorate = inPhrase;
		this.lilypondOutput = new String();
		this.outputBuffer = new StringBuffer();
		this.writeStaff(inPhrase);
	}

	public String output() {
		// TODO Auto-generated method stub
		return this.outputBuffer.toString();
	}

	private void writeStaff(Phrase phrase) {
		this.outputBuffer.append("\\new Staff {\n");
		// This forces accidentals on each note
		this.outputBuffer.append("#(set-accidental-style 'dodecaphonic)\n");
		this.outputBuffer.append(LilypondPhraseDecorator.DEFAULT_CLEF);
		/*
		 * \new Voice \with { \remove "Note_heads_engraver" \consists
		 * "Completion_heads_engraver"
		 */
		this.outputBuffer.append("\\new Voice \\with {\n");
		this.outputBuffer.append("  \\remove \"Note_heads_engraver\"\n");
		this.outputBuffer
				.append("  \\consists \"Completion_heads_engraver\"\n");
		// closing } for Voice
		this.outputBuffer.append("\n}\n");
		// store the initial offset from the Phrase's Timepoint into startRest
		double startRest = phrase.getTimepoint().getTime();
		List<SynchronousEventInterface> se = phrase.getSynchronousEvents();
		Iterator<SynchronousEventInterface> eventIt = phrase.getSynchronousEvents()
				.iterator();
		// suck up all the rest events at the beginning of a phrase. there
		// generally should only be one, but
		// no reason there can't be more than one. Add all the durations to
		// startRest to accumulate all of
		// the silent space at the beginning of a Phrase into one duration.
		boolean isRest = true;
		StringBuffer noteBuffer = new StringBuffer();
		while (eventIt.hasNext()) {
			SynchronousEventInterface ev = eventIt.next();
			if (ev.getClass().isAssignableFrom(SynchronousRestEvent.class)) {
				startRest += ev.getDuration();
			} else {
				SynchronousNoteEvent ne = (SynchronousNoteEvent) ev;
				// store the first NoteEvent Lilypond string in noteBuffer
				int count = 0;
				/*
				for ( Integer noteNum : ne.getPitchSet().getMembers() ) {
					noteBuffer.append(LilypondUtils.getNoteValue(ne.getTimepoint(),
						ne.getDuration(), noteNum, "s"));
					if ( count + 1 < ne.getPitchSet().size() ) {
						noteBuffer.append( " ");
					}
				}
				*/
/*
				noteBuffer.append(LilypondUtils.getNoteValue(ne.getTimepoint(),
					ne.getDuration(), ne.getPitchSet().getMembers(), "s"));
				if ( count + 1 < ne.getPitchSet().size() ) {
					noteBuffer.append( " ");
				}
*/
				break;
			}
		}
		// Write out the rest value of startRest into the buffer if the Phrase
		// begins with a rest.
		// If the Phrase begins with a rest, startRest will be > 0.
		if (startRest > 0) {
			this.outputBuffer.append(LilypondUtils.getRestValue(new Timepoint(
					startRest)));
		}
		// write out stored noteBuffer into the outputBuffer
		this.outputBuffer.append(noteBuffer);
		while (eventIt.hasNext()) {
			SynchronousEventInterface be = eventIt.next();

/*
			if (be instanceof NoteEvent) {
				SynchronousNoteEvent ne = (SynchronousNoteEvent) be;
				this.outputBuffer
						.append(LilypondUtils.getNoteValue(ne.getTimepoint(),
								ne.getDuration(), ne.getPitchSet().getMembers(), "s"));
			} else if (be instanceof SynchronousRestEvent) {
				this.outputBuffer.append(LilypondUtils.getRestValue(
						be.getTimepoint(), be.getDuration()));
			}
*/
		}
		// closing } for Staff
		this.outputBuffer.append("\n}\n");
	}
}
