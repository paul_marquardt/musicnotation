package com.newscores.notation.decorators;

import com.newscores.notation.interfaces.ObjectDecoratorInterface;
import com.newscores.setTheory.CompositionMatrix;
import com.newscores.setTheory.CMSegment;
import com.newscores.setTheory.PitchClassSequence;
import java.util.*;

public class LilypondAbstractCMDecorator implements ObjectDecoratorInterface {

	// private Phrase phraseToDecorate;
	public static final String TREBLE_CLEF = "\\clef treble\n";
	public static final String BASS_CLEF = "\\clef bass\n";
	public static final String DEFAULT_CLEF = TREBLE_CLEF;
	public static final String DEFAULT_ACCIDENTAL = "s";
	private StringBuffer outputBuffer;

	public LilypondAbstractCMDecorator(CompositionMatrix cm, String desc ) {
		this.outputBuffer = new StringBuffer();
		this.writeStaves(cm, desc);
	}

	public String output() {
		// TODO Auto-generated method stub
		return this.outputBuffer.toString();
	}

	private void writeStaves( CompositionMatrix cm, String description ) {
		// write the Phrase description as a comment.

		this.outputBuffer.append("#(set-accidental-style 'dodecaphonic)\n");
		this.outputBuffer.append(LilypondAbstractPhraseDecorator.DEFAULT_CLEF);
		/*
		 * \new Voice \with { \remove "Note_heads_engraver" \consists
		 * "Completion_heads_engraver"
		 */
		this.outputBuffer.append("\\new Voice \\with {\n");
		this.outputBuffer.append("  \\remove \"Note_heads_engraver\"\n");
		this.outputBuffer
				.append("  \\consists \"Completion_heads_engraver\"\n");
		// closing } for Voice
		this.outputBuffer.append(
				"  \\once \\override Staff.TimeSignature #'stencil = ##f\n"
		);
		this.outputBuffer.append("\n}\n");
		this.outputBuffer.append( "\\hide Stem\n" );
		this.outputBuffer.append("\n");
		this.outputBuffer.append( "\\layout {\n" );
		this.outputBuffer.append( "  \\context {\n" );
		this.outputBuffer.append( "	    \\Staff\n" );
		this.outputBuffer.append( "	    \\remove Time_signature_engraver\n" );
		this.outputBuffer.append( "	 }\n" );
		this.outputBuffer.append( "}\n" );

	}

}
