package com.newscores.notation.decorators;

import java.util.Iterator;
import java.util.List;

import com.newscores.notation.model.Phrase;
import com.newscores.notation.interfaces.SynchronousEventInterface;
import com.newscores.notation.interfaces.ObjectDecoratorInterface;

public class LilypondAbstractPhraseDecorator implements ObjectDecoratorInterface {

	// private Phrase phraseToDecorate;
	public static final String TREBLE_CLEF = "\\clef treble\n";
	public static final String BASS_CLEF = "\\clef bass\n";
	public static final String DEFAULT_CLEF = TREBLE_CLEF;
	public static final String DEFAULT_ACCIDENTAL = "s";
	private String lilypondOutput;
	private StringBuffer outputBuffer;
	private Phrase phraseToDecorate;

	public LilypondAbstractPhraseDecorator(List<Phrase> inPhraseList) {
		// this.phraseToDecorate = inPhrase;
		this.lilypondOutput = new String();
		this.outputBuffer = new StringBuffer();
		this.writeStaff(inPhraseList);
	}

	public String output() {
		// TODO Auto-generated method stub
		return this.outputBuffer.toString();
	}

	private void writeStaff( List<Phrase> inPhraseList ) {
		this.outputBuffer.append("\\new Staff {\n");
		double defaultDuration = 1.0; // in this notation, everything's a quarter note.
		// This forces accidentals on each note
		this.outputBuffer.append("#(set-accidental-style 'dodecaphonic)\n");
		this.outputBuffer.append(LilypondAbstractPhraseDecorator.DEFAULT_CLEF);
		/*
		 * \new Voice \with { \remove "Note_heads_engraver" \consists
		 * "Completion_heads_engraver"
		 */
		this.outputBuffer.append("\\new Voice \\with {\n");
		this.outputBuffer.append("  \\remove \"Note_heads_engraver\"\n");
		this.outputBuffer
				.append("  \\consists \"Completion_heads_engraver\"\n");
		// closing } for Voice
		this.outputBuffer.append(
				"  \\once \\override Staff.TimeSignature #'stencil = ##f\n"
		);
		this.outputBuffer.append("\n}\n");
		this.outputBuffer.append( "\\hide Stem\n" );
		// store the initial offset from the Phrase's Timepoint into startRest
		int currentPhrase = 1;
		for( Phrase phrase : inPhraseList ) {
			List<SynchronousEventInterface> se = phrase.getSynchronousEvents();
			Iterator<SynchronousEventInterface> eventIt = phrase.getSynchronousEvents()
					.iterator();
			// suck up all the rest events at the beginning of a phrase. there
			// generally should only be one, but
			// no reason there can't be more than one. Add all the durations to
			// startRest to accumulate all of
			// the silent space at the beginning of a Phrase into one duration.
			StringBuffer noteBuffer = new StringBuffer();
			if ( currentPhrase == 1 ) {
				noteBuffer.append( String.format("\\time %d/4\n", se.size() ) );
			}
			currentPhrase++;
			// Write out the rest value of startRest into the buffer if the Phrase
			// begins with a rest.
			// If the Phrase begins with a rest, startRest will be > 0.
			// write out stored noteBuffer into the outputBuffer
			this.outputBuffer.append(noteBuffer);
			while (eventIt.hasNext()) {
				SynchronousEventInterface be = eventIt.next();
/*
				if (be instanceof NoteEvent) {
					SynchronousNoteEvent ne = (SynchronousNoteEvent) be;
					this.outputBuffer
							.append(LilypondUtils.getNoteValue(new Timepoint(0),
									defaultDuration, ne.getPitchSet().getMembers(), "s") );
				} else if (be instanceof SynchronousRestEvent) {
					this.outputBuffer.append(" s " );
				}
*/
			}
			this.outputBuffer.append("\n");
			// closing } for Staff
		}
		this.outputBuffer.append( "\n}\n" );
		/*
		this.outputBuffer.append( "\\layout {\n" );
		this.outputBuffer.append( "  \\context {\n" );
		this.outputBuffer.append( "	    \\Staff\n" );
		this.outputBuffer.append( "	    \\remove Time_signature_engraver\n" );
		this.outputBuffer.append( "	 }\n" );
		this.outputBuffer.append( "}\n" );
		*/
	}
}
