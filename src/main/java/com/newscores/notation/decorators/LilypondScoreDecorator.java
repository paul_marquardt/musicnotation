package com.newscores.notation.decorators;

import com.newscores.notation.interfaces.ObjectDecoratorInterface;

import java.util.Iterator;
import java.util.List;

public class LilypondScoreDecorator implements ObjectDecoratorInterface {

	// private Phrase phraseToDecorate;
	public static final String TREBLE_CLEF = "\\clef treble\n";
	public static final String BASS_CLEF = "\\clef bass\n";
	public static final String DEFAULT_CLEF = TREBLE_CLEF;
	public static final String DEFAULT_ACCIDENTAL = "s";
	// private ObjectDecorator interiorDecorator;
	private List<ObjectDecoratorInterface> decorators;
	private boolean suppressTimeSignatures = false;
	
	public LilypondScoreDecorator(String scoreName,
			List<ObjectDecoratorInterface> decoratorList) {
		// this.phraseToDecorate = inPhrase;
		// this.interiorDecorator = someDecorator;
		this.decorators = decoratorList;
	}

	public LilypondScoreDecorator(String scoreName,
			List<ObjectDecoratorInterface> decoratorList, boolean suppressTimeSignatures ) {
		// this.phraseToDecorate = inPhrase;
		// this.interiorDecorator = someDecorator;
		this.decorators = decoratorList;
		this.suppressTimeSignatures = suppressTimeSignatures;
	}

	public String getScoreHeader() {
		return ("\\version \"2.8.1\"\n");
	}

	public Iterator<ObjectDecoratorInterface> decoratorIterator() {
		return this.decorators.iterator();
	}

	public List<ObjectDecoratorInterface> getDecorators() {
		return this.decorators;
	}

	public String getScoreFooter() {
		// this.outputBuffer.append(">>");
		return ("\n");
	}

	public String output() {
		// TODO Auto-generated method stub
		StringBuffer outBuf = new StringBuffer();
		outBuf.append( this.getScoreHeader() );
		Iterator<ObjectDecoratorInterface> objDecIt = this.decoratorIterator();
		while ( objDecIt.hasNext() ) {
			outBuf.append( objDecIt.next().output() );
		}
		outBuf.append( this.getScoreFooter() );
		if ( this.suppressTimeSignatures ) {
			outBuf.append( "\\layout {\n" );
			outBuf.append( "  \\context {\n" );
			outBuf.append( "	    \\Staff\n" );
			outBuf.append( "	    \\remove Time_signature_engraver\n" );
			outBuf.append( "	 }\n" );
			outBuf.append( "}\n" );
		}
		return outBuf.toString();
	}

}
