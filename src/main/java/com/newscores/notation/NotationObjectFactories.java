package com.newscores.notation;

import com.newscores.notation.model.*;

import java.util.List;

public class NotationObjectFactories {

	public static Phrase getPhrase() {
		return new Phrase();
	}

	public static Phrase getPhrase(Timepoint inTimepoint) {
		return new Phrase(inTimepoint);
	}

	public static Phrase getPhrase(Timepoint inTimepoint,
			List<BaseEvent> inEvents) {
		return new Phrase(inTimepoint, inEvents);
	}

	public static NoteEvent getNoteEvent() {
		return new NoteEvent();
	}

	public static RestEvent getRestEvent() {
		return new RestEvent();
	}

	public static Timepoint getTimepoint() {
		return new Timepoint();
	}

	public static Timepoint getTimepoint(double startTime) {
		return new Timepoint(startTime);
	}

}
