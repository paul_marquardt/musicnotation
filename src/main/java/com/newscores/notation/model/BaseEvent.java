package com.newscores.notation.model;


import com.newscores.notation.interfaces.EventInterface;

public class BaseEvent implements EventInterface {
	public static final double DEFAULT_DURATION = 1.0;

	private double duration;

	public double getDuration() {
		return this.duration;
	}

	public void setDuration(double duration) {
	this.duration = duration;
	}
}
