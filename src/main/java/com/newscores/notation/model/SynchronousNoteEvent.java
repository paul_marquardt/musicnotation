package com.newscores.notation.model;

import com.newscores.notation.interfaces.SynchronousEventInterface;
import com.newscores.notation.model.Timepoint;

public class SynchronousNoteEvent extends NoteEvent implements SynchronousEventInterface {

	private Timepoint timepoint;

	public SynchronousNoteEvent() {
		super(NoteEvent.DEFAULT_PITCH_STRING, BaseEvent.DEFAULT_DURATION, NoteEvent.DEFAULT_INTENSITY );
		this.setTimepoint(new Timepoint());
		// TODO Auto-generated constructor stub
	}

	public SynchronousNoteEvent(double inDuration) {
		super(NoteEvent.DEFAULT_PITCH_STRING, inDuration, NoteEvent.DEFAULT_INTENSITY );
		this.setTimepoint(new Timepoint());		
		// TODO Auto-generated constructor stub
	}

	public SynchronousNoteEvent(double inDuration, Timepoint inTimepoint) {
		super(NoteEvent.DEFAULT_PITCH_STRING, inDuration, NoteEvent.DEFAULT_INTENSITY );
		this.setTimepoint(inTimepoint);
		// TODO Auto-generated constructor stub
	}

	public SynchronousNoteEvent(String inPitchString, double inDuration, Timepoint inTimepoint) {
		super(inPitchString, inDuration, NoteEvent.DEFAULT_INTENSITY );
		this.setTimepoint(inTimepoint);
		// TODO Auto-generated constructor stub
	}

	public SynchronousNoteEvent(String inPitchString, double inDuration, Timepoint inTimepoint,
								 int inIntensity) {
		super( inPitchString, inDuration, inIntensity);
		this.setTimepoint(inTimepoint);
		// TODO Auto-generated constructor stub
	}


	public Timepoint getTimepoint() {
		return timepoint;
	}

	public void setTimepoint(Timepoint timepoint) {
		this.timepoint = timepoint;
	}
}
