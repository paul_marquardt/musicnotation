package com.newscores.notation.model;

import com.newscores.notation.NotationObjectFactories;
import com.newscores.notation.interfaces.PhraseInterface;
import com.newscores.notation.interfaces.SynchronousEventInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

//import java.util.logging.Logger;

public class Phrase implements PhraseInterface, Iterable<BaseEvent>, Cloneable {

	private Timepoint timepoint;
	private List<BaseEvent> events;
	private double duration;
	public static final double TIMEPOINT_INCREMENT = 0.25;
	private String description;
	private double timepointIncrement;
	private long hashValue;

	public Phrase() {
		this(new Timepoint(0.0));
		// TODO Auto-generated constructor stub
	}

	public Phrase(Timepoint inTimepoint) {
		this(inTimepoint, new ArrayList<BaseEvent>());
		// TODO Auto-generated constructor stub
	}

	public Phrase(Timepoint inTimepoint, List<BaseEvent> inEvents) {
		this(inTimepoint, inEvents, Phrase.TIMEPOINT_INCREMENT);
	}

	public Phrase(Timepoint inTimepoint, List<BaseEvent> inEvents,
			double increment) {
		super();
		this.setTimepoint(inTimepoint);
		this.events = inEvents;
		this.timepointIncrement = increment;
		//logger = LoggerFactory.getLogger(Phrase.class);
	}

	// noteAtTimepoint( Timepoint timepoint ) returns the event that is occuring
	// at the Timepoint passed in.
	// "Occuring at" means that the Event start time is equal to or less than
	// the Timepoint time and
	// Timepoint time is less than the next event's start time.
	public BaseEvent getEventAtTimepoint(Timepoint timepoint) {
		double currentEventTime = this.getTimepoint().getTime();
		// System.out.println(
		// String.format("Starting getEventTimepoint: currentEventTime is %f",
		// currentEventTime ) );
		Iterator<BaseEvent> eventIt = this.events.iterator();
		while (eventIt.hasNext()) {
			BaseEvent event = eventIt.next();
			// System.out.println(
			// String.format("In getEventTimepoint: currentEventTime is %f",
			// currentEventTime ) );
			if (currentEventTime <= timepoint.getTime()
					&& timepoint.getTime() < currentEventTime
							+ event.getDuration()) {
				return event;
			}
			currentEventTime = currentEventTime + event.getDuration();
		}
		return null;
	}

	public NoteEvent getNoteEventAtTimepoint(Timepoint inTimepoint) {
		double currentEventTime = this.getTimepoint().getTime();
		// System.out.println(
		// String.format("Starting getEventTimepoint: currentEventTime is %f",
		// currentEventTime ) );
		Iterator<BaseEvent> eventIt = this.events.iterator();
		while (eventIt.hasNext()) {
			BaseEvent event = eventIt.next();
			// System.out.println(
			// String.format("In getEventTimepoint: currentEventTime is %f",
			// currentEventTime ) );
			// System.out.println( "Event: " + event.toString() );
			if (inTimepoint.getTime() >= currentEventTime
					&& inTimepoint.getTime() < currentEventTime
							+ event.getDuration()
					&& event.getClass().isAssignableFrom(NoteEvent.class)) {
				return (NoteEvent) event;
			}
			currentEventTime += event.getDuration();
		}
		return null;
	}

	public List<SynchronousEventInterface> getSynchronousEvents() {
		List<SynchronousEventInterface> syncEvents = new ArrayList<>();
		Iterator<BaseEvent> eventIt = this.events.iterator();
		double durationCounter = this.timepoint.getTime();
		while (eventIt.hasNext()) {
			BaseEvent be = eventIt.next();
/*
			if (be instanceof NoteEvent) {
				NoteEvent ne = (NoteEvent) be;
				syncEvents.add(new SynchronousNoteEvent( ne.getDuration(), new Timepoint(durationCounter), ne.getPitchList() ) );
			}
			if (be instanceof RestEvent) {
				syncEvents.add(new SynchronousRestEvent((RestEvent) be,
						new Timepoint(durationCounter)));
			}
*/
			durationCounter += be.getDuration();
		}
		return syncEvents;
	}

	// addEvent adds an Event to the end of the event list. It knows nothing about timepoints.
	/**
	public boolean addEvent(BaseEvent eventToAdd ) {
		if (eventToAdd instanceof NoteEvent ) {
			this.addEvent(eventToAdd);
		}
		if (eventToAdd instanceof RestEvent ) {
			this.addEvent(eventToAdd);
		}
		return false;
	}
	**/

	// addEvent adds an Event to the end of the event list. It knows nothing about timepoints.
	public boolean addEvent(RestEvent eventToAdd) {
		if (eventToAdd != null && this.events.add(eventToAdd)) {
			this.duration += eventToAdd.getDuration();
			return true;
		}
		return false;
	}

	// addEvent adds an Event to the end of the event list. It knows nothing about timepoints.
	public boolean addEvent(NoteEvent eventToAdd) {
		if (eventToAdd != null && this.events.add(eventToAdd)) {
			this.duration += eventToAdd.getDuration();
			return true;
		}
		return false;
	}

	public boolean removeEventAtTimepoint(Timepoint timepoint) {
		double currentEventTime = this.getTimepoint().getTime();
		for (int index = 0; index < this.events.size(); index++) {
			// need to store the duration because we might delete the event and
			// need to capture the duration.
			double eventDuration = this.events.get(index).getDuration();
			if (currentEventTime <= timepoint.getTime()
					&& timepoint.getTime() < currentEventTime + eventDuration) {
				this.events.remove(index);
			}
			currentEventTime += eventDuration;
		}
		return false;
	}

	public Timepoint getTimepoint() {
		return timepoint;
	}

	public void setTimepoint(Timepoint timepoint) {
		this.timepoint = timepoint;
	}

	public double getDuration() {
		// TODO Auto-generated method stub
		double phraseDuration = 0.0;
		Iterator<BaseEvent> eventIt = this.events.iterator();
		while (eventIt.hasNext()) {
			phraseDuration += eventIt.next().getDuration();
		}
		return phraseDuration;
	}

	// getEndTimepoint returns a Timepoint representing the time when the last
	// NoteEvent of the phrase ends.
	public Timepoint getEndTimepoint() {
		// Timepoint endTimepoint;
		// TODO needs to be fixed for the case that the last Events in the
		// Phrase are RestEvents, in which case
		// they do not contribute to the duration.
		// return new Timepoint( this.getTimepoint().getTime() +
		// this.getDuration() );
		return NotationObjectFactories.getTimepoint(this.getTimepoint().getTime()
				+ this.getDuration());

	}

	public Phrase transpose(int transposition) {
		// Phrase newPhrase = new Phrase();
		Phrase newPhrase = NotationObjectFactories.getPhrase();
		newPhrase.setTimepoint(this.getTimepoint());
		Iterator<BaseEvent> eventIt = this.events.iterator();
		/**  **/
/*
		while (eventIt.hasNext()) {
			BaseEvent event = eventIt.next();
			if (event instanceof NoteEvent) {
				// NoteEvent newEvent = new NoteEvent();
				NoteEvent newEvent = NotationObjectFactories.getNoteEvent();
				newEvent.setPitchSet( ((NoteEvent) event).getPitchSet().T(transposition) );
				newEvent.setDuration(event.getDuration());
				newEvent.setIntensity(((NoteEvent) event).getIntensity());
				newPhrase.addEvent(newEvent);
			} else if (event instanceof RestEvent) {
				newPhrase.addEvent((RestEvent) event);
			}
		}
*/
		return newPhrase;
	}

	/*
	 * The inversion method returns a inverted transformation of a Phrase.
	 * Inversion is defined here as in inversion in Pitch Space where the Phrase
	 * inverted around the first note, so that the first note of a Phrase and
	 * its inversion will always be equal.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.marqrdt.counterpoint.AbstractPhrase#inversion()
	 */
	public Phrase inversion() {
		Phrase newPhrase = NotationObjectFactories.getPhrase();
		newPhrase.setTimepoint(this.getTimepoint());
		Iterator<BaseEvent> eventIt = this.events.iterator();
		// an implausible first note...
		// Is used as a flag to indicate that the iteration is processing the
		// first note event.
		int firstNote = -9999;

/*
		while (eventIt.hasNext()) {
			BaseEvent event = eventIt.next();
			if (event instanceof NoteEvent) {
				if (firstNote == -9999) {
					firstNote = ((NoteEvent) event).getPitchSet().minPitch();
				}
				// NoteEvent newEvent = new NoteEvent();
				NoteEvent newEvent = NotationObjectFactories.getNoteEvent();
				newEvent.setPitchSet( ((NoteEvent) event).getPitchSet().I() );
				newEvent.setDuration(event.getDuration());
				newEvent.setIntensity(((NoteEvent) event).getIntensity());
				newPhrase.addEvent(newEvent);
			} else if (event instanceof RestEvent) {
				newPhrase.addEvent((RestEvent) event);
			}
		}
*/
		return newPhrase;
	}

	public Phrase retrograde() {
		ArrayList<BaseEvent> eventsAsArrayList = new ArrayList<BaseEvent>(
				this.events);
		Collections.reverse(eventsAsArrayList);
		List<NoteEvent> notes = this.getNoteEvents();
		if (notes.size() > 0) {
			this.getTimepoint();
		}
		// return new Phrase( this.getTimepoint(), eventsAsArrayList );
		return NotationObjectFactories
				.getPhrase(this.getTimepoint(), eventsAsArrayList);
	}

	public Phrase offset(double amountToOffset) {
		return new Phrase(new Timepoint(this.getTimepoint().getTime()
				+ amountToOffset), this.getEvents());
	}

	public List<BaseEvent> getEvents() {
		return events;
	}

	// returns an ordered List of all NoteEvents, i.e. the caller can guarantee
	// that each event is
	// a NoteEvent and can access pitch and intensity. No Timepoint information
	// is included in a
	// NoteEvent, so this method is mostly useful for inspecting pitch elements.
	public List<NoteEvent> getNoteEvents() {
		// Phrase newPhrase = new Phrase();
		List<NoteEvent> noteEvents = new ArrayList<NoteEvent>();
		Iterator<BaseEvent> eventIt = this.events.iterator();
		while (eventIt.hasNext()) {
			BaseEvent event = eventIt.next();
			if (event instanceof NoteEvent) {
				// NoteEvent newEvent = new NoteEvent();
				noteEvents.add((NoteEvent) event);
			}
		}
		return noteEvents;
	}

	public boolean hasAttackAtTimepoint(Timepoint inTp) {
		List<SynchronousEventInterface> synchronousEvents = this.getSynchronousEvents();
		// loop through all SynchronousEvents
		for (SynchronousEventInterface se : synchronousEvents) {
			// If it is a NoteEvent, i.e. has a pitch element
			if (se.getClass().isAssignableFrom(NoteEvent.class)) {
				// If the elements Timepoint equals the Timepoint passed in,
				// there is a sounded attack at this point
				// RestEvents beginning at a Timepoint do not qualify as an
				// attack.
				if (inTp.getTime() == se.getTimepoint().getTime()) {
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * getIntervalAtTimepoint is a helper method that returns the interval at
	 * the Timepoint passed in. It is a measurement of the Pitch Space interval
	 * between two pitch events. If there is note playing at the Timepoint, but
	 * no change of pitch, it returns 0. If there is a change in pitch, it
	 * returns the the value of pitch1 - pitch2. For example, for the interval
	 * of a downward perfect fifth, the method will return -7. For a rising
	 * major third, it will return 4. If there is no pitch sounding at
	 * Timepoint, it returns null, so calls to this method must check for null.
	 */
	public Integer getIntervalAtTimepoint(Timepoint inTp) {
		ArrayList<NoteEvent> events = new ArrayList<NoteEvent>();
		if (this.getNoteEventAtTimepoint(inTp) == null) {
			return null;
		}
		ArrayList<SynchronousEventInterface> synchronousEvents = (ArrayList<SynchronousEventInterface>) this.getSynchronousEvents();
		if (!this.hasAttackAtTimepoint(inTp)) {
			return 0;
		}

		return null;
	}

	public void setEvents(List<BaseEvent> events) {
		this.events = events;
	}

	@Override
	public boolean phraseEquals(PhraseInterface anotherPhrase) {
		return false;
	}

	public boolean phraseEquals(Phrase anotherPhrase) {
		boolean retVal = true;
		// basic checks. If any fail, immediately return false and skip
		// subsequent tests.
		if (this.getEvents().size() != anotherPhrase.getEvents().size()) {
			// System.out.println(
			// String.format("Number of elements in phrases do not match: %d :: %d.",
			// this.getEvents().size(), anotherPhrase.getEvents().size() ) );
			// return false;
			return false;
		}
		if (!this.getTimepoint().equals(anotherPhrase.getTimepoint())) {
			// return false;
			return false;
		}
		if (this.getEvents().size() != anotherPhrase.getEvents().size()) {
			// return false;
			return false;
		}
		for (int index = 0; index < this.getEvents().size(); index++) {
			if (this.getEvents().get(index).getClass()
					.isAssignableFrom(NoteEvent.class)
					&& anotherPhrase.getEvents().get(index).getClass()
							.isAssignableFrom(NoteEvent.class)) {
				NoteEvent thisNoteEvent = (NoteEvent) this.getEvents().get(
						index);
				NoteEvent anotherPhraseNoteEvent = (NoteEvent) anotherPhrase
						.getEvents().get(index);
				/**
				 * System.out.println( String.format(
				 * "Phrase elements are not equal:\n%s%s",
				 * thisNoteEvent.toString(), anotherPhraseNoteEvent.toString() )
				 * );
				 **/
				if (!thisNoteEvent.eventEquals(anotherPhraseNoteEvent)) {
					return false;
				}
			} else if (this.getEvents().get(index).getClass()
					.isAssignableFrom(RestEvent.class)
					&& anotherPhrase.getEvents().get(index).getClass()
							.isAssignableFrom(RestEvent.class)) {
				RestEvent thisRestEvent = (RestEvent) this.getEvents().get(
						index);
				RestEvent anotherPhraseRestEvent = (RestEvent) anotherPhrase
						.getEvents().get(index);
				/**
				 * System.out.println( String.format(
				 * "Phrase elements are not equal:\n%s%s",
				 * thisRestEvent.toString(), anotherPhraseRestEvent.toString() )
				 * );
				 **/
				if (!thisRestEvent.eventEquals(anotherPhraseRestEvent)) {
					return false;
				}
			} else {
				return false;
			}
		}
		// return true;
		return retVal;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(String
				.format("Phrase\nTimepoint: %f\nDuration: %f\nEnd timepoint: %f\nEvents:\n",
						this.getTimepoint().getTime(), this.getDuration(), this
								.getEndTimepoint().getTime()));
		Iterator<BaseEvent> eventIt = this.events.iterator();
		while (eventIt.hasNext()) {
			BaseEvent event = eventIt.next();
			sb.append("\t" + event.toString() + "\n ");
		}
		return sb.toString();
	}

	public String asTimeline() {
		StringBuffer sb = new StringBuffer();
		return sb.toString();
	}

	public void setDuration(double duration) {
		// TODO Auto-generated method stub

	}

	public double getTimepointIncrement() {
		return this.timepointIncrement;
	}

	public void setTimepointIncrement(double increment) {
		// TODO Auto-generated method stub

	}

	public SynchronousNoteEvent getHighestNote() {
		SynchronousNoteEvent highestNote = new SynchronousNoteEvent();
/*
		highestNote.addPitch( Integer.MIN_VALUE );
		for ( SynchronousEvent event : this.getSynchronousEvents() ) {
			if ( event.getClass().isAssignableFrom( NoteEvent.class ) ) {
				SynchronousNoteEvent ne = (SynchronousNoteEvent) event;
				if ( ne.getPitchSet().maxPitch() > highestNote.getPitchSet().maxPitch() ) {
					highestNote = ne;
				}
			}
		}
*/
		return highestNote;
	}

	public SynchronousNoteEvent getLowestNote() {
		SynchronousNoteEvent lowestNote = new SynchronousNoteEvent();
/*
		lowestNote.addPitch( Integer.MAX_VALUE );
		for ( SynchronousEvent event : this.getSynchronousEvents() ) {
			if ( event.getClass().isAssignableFrom( NoteEvent.class ) ) {
				SynchronousNoteEvent ne = (SynchronousNoteEvent) event;
				if ( ne.getPitchSet().minPitch() < lowestNote.getPitchSet().minPitch() ) {
					lowestNote = ne;
				}
			}
		}
*/
		return lowestNote;
	}

	public List<String> getPitchList() {
		List<String> pitchList = new ArrayList<String>();
/*
		for ( SynchronousEvent event : this.getSynchronousEvents() ) {
			if ( event.getClass().isAssignableFrom( NoteEvent.class ) ) {
				newSet.extend( ((NoteEvent) event).getPitchSet() );
			}
		}
*/
		return pitchList;
	}
	
	public String getDescription() {
		// TODO Auto-generated method stub
		return this.description;
	}

	public void setDescription(String phraseDescription) {
		// TODO Auto-generated method stub
		this.description = phraseDescription;
	}

	public Iterator<BaseEvent> iterator() {
		// TODO Auto-generated method stub
		return this.events.iterator();
	}

	public long computeHash() {
		long hash = 0;
		return hash;
	}

	private void setHashValue(long hashValue) {
		this.hashValue = hashValue;
	}

}
