package com.newscores.notation.model;

public class Timepoint implements Comparable<Timepoint> {

	private double time;

	public Timepoint() {
		this.setTime(0.0);
	}

	public Timepoint(double startTime) {
		this.setTime(startTime);
	}

	public boolean equals(Timepoint anotherTimepoint) {
		if (this.getTime() == anotherTimepoint.getTime()) {
			return true;
		}
		return false;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public int compareTo(Timepoint inTp) {
		// TODO Auto-generated method stub
		if (this.getTime() < inTp.getTime()) {
			return -1;
		}
		if (this.getTime() > inTp.getTime()) {
			return 1;
		}
		return 0;
	}

}
