package com.newscores.notation.model;

public class RestEvent extends BaseEvent implements Cloneable {

	protected double duration;

	// protected String name;

	public RestEvent() {
		try {
			super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Use default duration of 1.0 (quarter note).
		this.setDuration(BaseEvent.DEFAULT_DURATION);
		// TODO Auto-generated constructor stub
	}

	public RestEvent(double inDuration) {
		this.setDuration(inDuration);
		// TODO Auto-generated constructor stub
	}

	public boolean eventEquals(RestEvent thing) {
		return this.getDuration() == thing.getDuration();
		// System.out.println("\n");
	}

	@Override
	public RestEvent clone() {
		try {
			super.clone();
		} catch (CloneNotSupportedException exc) {

		}
		RestEvent ne = new RestEvent();
		ne.setDuration(this.getDuration());
		return ne;
	}

	public double getDuration() {
		return this.duration;
	}

	public void setDuration(double inDuration) {
		// TODO Auto-generated method stub
		this.duration = inDuration;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.getClass().getName()
				.substring(this.getClass().getName().lastIndexOf('.') + 1)
				+ ": ");
		// sb.append("\n");
		sb.append(String.format("duration: %f", this.getDuration()));
		return sb.toString();
	}
	/**
	 * public String getName() { // TODO Auto-generated method stub return
	 * this.name; }
	 * 
	 * public void setName(String inName) { // TODO Auto-generated method stub
	 * this.name = inName; }
	 **/
}