package com.newscores.notation.model;

import com.newscores.notation.interfaces.SynchronousEventInterface;

public class SynchronousRestEvent extends BaseEvent implements SynchronousEventInterface {

	private Timepoint timepoint;
	protected double duration;
	protected String name;

	public SynchronousRestEvent(double inDuration) {
		this.setDuration(inDuration);
		this.setTimepoint(new Timepoint());
		// TODO Auto-generated constructor stub
	}

	public SynchronousRestEvent(double inDuration, Timepoint inTimepoint) {
		this.setDuration(inDuration);
		this.setTimepoint(inTimepoint);
		// TODO Auto-generated constructor stub
	}

	public SynchronousRestEvent(RestEvent re, Timepoint inTimepoint) {
		this.setDuration(re.duration);
		this.setTimepoint(inTimepoint);
		// TODO Auto-generated constructor stub
	}

	public Timepoint getTimepoint() {
		return timepoint;
	}

	public void setTimepoint(Timepoint timepoint) {
		this.timepoint = timepoint;
	}

	public void setDuration(double inDuration) {
		this.duration = inDuration;
	}

	public double getDuration() {
		return this.duration;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setName(String name) {
		// TODO Auto-generated method stub

	}
}
