package com.newscores.notation.model;

import java.util.ArrayList;
import java.util.List;

public class NoteEvent extends BaseEvent implements Cloneable {

	protected int intensity;
	private List<String> pitchList;
	protected double duration;
	public static final int DEFAULT_PITCH = 60;
	public static String DEFAULT_PITCH_STRING = "c";
	public static final int DEFAULT_INTENSITY = 50;
	
	public NoteEvent() {
		this( DEFAULT_PITCH_STRING, DEFAULT_DURATION, DEFAULT_INTENSITY);
	}

	public NoteEvent(double inDuration) {
		this( DEFAULT_PITCH_STRING, inDuration, DEFAULT_INTENSITY);
	}

	public NoteEvent(double inDuration, String pitchString) {
		this( pitchString, inDuration, DEFAULT_INTENSITY);
	}

	public NoteEvent(String pitchString, double inDuration, int inIntensity) {
		setDuration( inDuration );
		setIntensity( inIntensity );
		this.pitchList = new ArrayList<String>();
		this.pitchList.add(pitchString);
		//new NoteEvent( inDuration, newSet, inIntensity );
		// TODO Auto-generated constructor stub
	}

	public void addPitches(List<String> pitchesToAdd) {
		this.pitchList.addAll( pitchesToAdd );
	}

	public List<String> getPitchList() {
		return this.pitchList;
	}

	public double getDuration() {
		return this.duration;
	}


	public int getIntensity() {
		return intensity;
	}

	public void setPitchList(List<String> pitchList) {
		this.pitchList = pitchList;
	}

	public void setDuration(double inDuration) {
		//System.out.println( String.format("Setting duration to %f", inDuration) );
		this.duration = inDuration;
		// TODO Auto-generated method stub
	}

	public void setIntensity(int intensity) {
		//System.out.println( String.format("Setting intensity to %d", intensity) );
		this.intensity = intensity;
	}

	public boolean eventEquals(NoteEvent thing) {
		return this.getDuration() == thing.getDuration()
				&& this.getIntensity() == thing.getIntensity()
				&& this.getPitchList().equals( thing.getPitchList() );
	}

	@Override
	public NoteEvent clone() {
		try {
			super.clone();
		} catch (CloneNotSupportedException exc) {

		}
		NoteEvent ne = new NoteEvent();
		ne.setDuration(this.getDuration());
		ne.setIntensity(this.getIntensity());
		ne.setPitchList( this.getPitchList() );
		return ne;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.getClass().getName()
				.substring(this.getClass().getName().lastIndexOf('.') + 1)
				+ ": ");
		sb.append(String.format("pitch: %s, ", this.getPitchList().toString() ) );
		sb.append(String.format("intensity: %d, ", this.getIntensity()));
		sb.append(String.format("duration: %f", this.getDuration()));
		return sb.toString();
	}

	/**
	 * public String getName() { // TODO Auto-generated method stub return null;
	 * }
	 * 
	 * public void setName(String name) { // TODO Auto-generated method stub
	 * 
	 * }
	 **/
}