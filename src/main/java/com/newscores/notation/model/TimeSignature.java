package com.newscores.notation.model;

import org.apache.commons.math3.fraction.Fraction;

public class TimeSignature {

    private Fraction timeSignature;
    private Fraction numerator;
    private int denominator;

    public TimeSignature( Fraction numerator, int denominator ) {
        this.numerator = numerator;
        this.denominator = denominator;
    }


}
