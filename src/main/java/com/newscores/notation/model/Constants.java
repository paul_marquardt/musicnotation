package com.newscores.notation.model;

public class Constants {

	public static int MEDIAN_PITCH = 60;
	public static Integer[] CHROMATIC_SCALE = {0,1,2,3,4,5,6,7,8,9,10,11};
	public static int CHROMATIC_SCALE_SIZE = CHROMATIC_SCALE.length;
}
