package com.newscores.notation.model;

public class PhraseConfig {

	public Integer maxDuration;
	public Integer minDuration;
	public Integer maxPitch;
	public Integer minPitch;
	public String pitchProfile;
	public String rhythmProfile;
	public Integer phraseLength;
	public Double phraseDensity;

}
