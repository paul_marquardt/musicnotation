package com.newscores.notation.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.newscores.notation.model.Timepoint;

public class LilypondUtils {

	public static Map<Double, String> restMap = new HashMap<Double, String>();
	static {
		Map<Double, String> tempMap = new HashMap<Double, String>();
		tempMap.put(0.125, "r32 ");
		tempMap.put(0.25, "r16 ");
		tempMap.put(0.375, "r16. ");
		tempMap.put(0.50, "r8 ");
		tempMap.put(0.625, "r8 r32 ");
		tempMap.put(0.75, "r8. ");
		tempMap.put(0.875, "r8 r16. ");
		tempMap.put(1.0, "r4 ");
		tempMap.put(2.0, "r2 ");
		tempMap.put(4.0, "r1 ");
		restMap = Collections.unmodifiableMap(tempMap);
	}

	public static Map<Double, String> noteValueMap = new HashMap<Double, String>();
	static {
		Map<Double, String> tempMap = new HashMap<Double, String>();
		tempMap.put(0.125, "32 ");
		tempMap.put(0.25, "16 ");
		tempMap.put(0.375, "16. ");
		tempMap.put(0.50, "8 ");
		tempMap.put(0.625, "8 ~ 32 ");
		tempMap.put(0.75, "8. ");
		tempMap.put(0.875, "8 ~ 16. ");
		tempMap.put(1.0, "4 ");
		tempMap.put(2.0, "2 ");
		noteValueMap = Collections.unmodifiableMap(tempMap);
	}

	public static String getPitchValue( Collection<Integer>pitches, String accidental ) {
		String valueString = "";
		StringBuffer pitchBuf = new StringBuffer(valueString);
		if ( pitches.size() > 1 ) {
			pitchBuf.append(" <");
		}
		//System.out.println( pitches.toString() );
		Iterator<Integer> pitchIt = pitches.iterator();
		while ( pitchIt.hasNext() ) {
			Integer pitchNum = pitchIt.next();
			//System.out.println( String.format("Getting value for pitch %s", pitchNum ) );			
			if (accidental.equalsIgnoreCase("s")) {
				switch (pitchNum % 12) {
				case 0:
					valueString = "c";
					break;
				case 1:
					valueString = "cis";
					break;
				case 2:
					valueString = "d";
					break;
				case 3:
					valueString = "dis";
					break;
				case 4:
					valueString = "e";
					break;
				case 5:
					valueString = "f";
					break;
				case 6:
					valueString = "fis";
					break;
				case 7:
					valueString = "g";
					break;
				case 8:
					valueString = "gis";
					break;
				case 9:
					valueString = "a";
					break;
				case 10:
					valueString = "ais";
					break;
				case 11:
					valueString = "b";
				}
			} else {
				switch (pitchNum % 12) {
				case 0:
					valueString = "c";
					break;
				case 1:
					valueString = "des";
					break;
				case 2:
					valueString = "d";
					break;
				case 3:
					valueString = "ees";
					break;
				case 4:
					valueString = "e";
					break;
				case 5:
					valueString = "f";
					break;
				case 6:
					valueString = "ges";
					break;
				case 7:
					valueString = "g";
					break;
				case 8:
					valueString = "aes";
					break;
				case 9:
					valueString = "a";
					break;
				case 10:
					valueString = "bes";
					break;
				case 11:
					valueString = "b";
				}
			}
			pitchBuf.append( valueString );
			if (pitchNum >= 60) {
				for (int i = 0; i < (pitchNum - 48) / 12; i++) {
					pitchBuf.append("'");
				}
			} else if (pitchNum < 48) {
				for (int i = 0; i < (60 - pitchNum) / 12; i++) {
					pitchBuf.append(",");
				}
			}
			// append a space if there are more pitches
			if ( pitchIt.hasNext() ) {
				pitchBuf.append(" ");
			}
		}
		// Enclose Lilypond chord notation with ">>"
		if ( pitches.size() > 1 ) {
			pitchBuf.append("> ");
		}		
		return pitchBuf.toString();
	}
	
	// calculates the LilyPond representation of a pitch/duration pair
	public static String getNoteValue(Timepoint inTp, double duration,
			Collection<Integer> pitches, String accidental) {
		String valueString = "";
		StringBuffer noteBuf = new StringBuffer();
		// get the pitch value from LilypondUtils.getPitchValue()
		noteBuf.append( LilypondUtils.getPitchValue( pitches, accidental) );
		String rhythmValue = "";
		double tempDuration = duration;
		// If the timepoint does not start on an integral value, get the
		// rhythmic value before the first whole beat.
		if ((int) inTp.getTime() != inTp.getTime()) {
			double headNoteValue = ((int) inTp.getTime() + 1) - inTp.getTime();
			// if the note duration is shorter or equal to headNoteValue, simply
			// return
			// the pitchString concatenated with the duration.
			if (tempDuration <= headNoteValue) {
				return noteBuf.append(LilypondUtils.noteValueMap.get(tempDuration)).toString();
			} else if (headNoteValue > 0
					&& LilypondUtils.noteValueMap.get(headNoteValue) != null) {
				noteBuf.append(LilypondUtils.noteValueMap.get(headNoteValue));
			}
			tempDuration -= headNoteValue;
			if (tempDuration > 0) {
				noteBuf.append("~ ");
			}
		}
		while (tempDuration > 1) {
			//if ((int) inTp.getTime() + 1 % 4 == 2 && tempDuration > 2.0) {
			if ((int) inTp.getTime() + 1 % 4 >= 2 && tempDuration > 2.0) {
				noteBuf.append(LilypondUtils.noteValueMap.get(2.0));
				tempDuration -= 2;
				if (tempDuration > 0) {
					noteBuf.append("~ ");
				}
			} else {
				noteBuf.append(LilypondUtils.noteValueMap.get(1.0));
				tempDuration -= 1;
				if (tempDuration > 0) {
					noteBuf.append("~ ");
				}
			}
		}
		if (tempDuration > 0
				&& LilypondUtils.noteValueMap.get(tempDuration) != null) {
			noteBuf.append(LilypondUtils.noteValueMap.get(tempDuration));
		}
		// noteBuf.append(" ");
		// Math.
		return noteBuf.toString();
	}

	// calculates the LilyPond representation of a pitch/duration pair using
	// a measure timepoint, or the timepoint in the measure at which the 
	// event starts.
	public static String getNoteValue(Timepoint inTp, double duration,
			Collection<Integer> pitches, String accidental, Timepoint measureTp) {
		String valueString = "";
		StringBuffer noteBuf = new StringBuffer();
		noteBuf.append( LilypondUtils.getPitchValue(pitches, accidental) );
		String rhythmValue = "";
		double measureTimepoint = 0.0;
		double tempDuration = duration;
		// If the timepoint does not start on an integral value, get the
		// rhythmic value before the first whole beat.
		if ((int) inTp.getTime() != inTp.getTime()) {
			double headNoteValue = ((int) inTp.getTime() + 1) - inTp.getTime();
			// if the note duration is shorter or equal to headNoteValue, simply
			// return
			// the pitchString concatenated with the duration.
			if (tempDuration <= headNoteValue) {
				return noteBuf.append(LilypondUtils.noteValueMap.get(tempDuration)).toString();
			} else if (headNoteValue > 0
					&& LilypondUtils.noteValueMap.get(headNoteValue) != null) {
				noteBuf.append( LilypondUtils.noteValueMap.get(headNoteValue) );
			}
			tempDuration -= headNoteValue;
			if (tempDuration > 0) {
				noteBuf.append("~ ");
			}
		}
		while (tempDuration > 1) {
			//if ((int) inTp.getTime() + 1 % 4 == 2 && tempDuration > 2.0) {
			if ((int) inTp.getTime() + 1 % 4 >= 2 && tempDuration > 2.0) {
				noteBuf.append( LilypondUtils.noteValueMap.get(2.0) );
				tempDuration -= 2;
				if (tempDuration > 0) {
					noteBuf.append("~ ");
				}
			} else {
				noteBuf.append(LilypondUtils.noteValueMap.get(1.0));
				tempDuration -= 1;
				if (tempDuration > 0) {
					noteBuf.append("~ ");
				}
			}
		}
		if (tempDuration > 0
				&& LilypondUtils.noteValueMap.get(tempDuration) != null) {
			noteBuf.append(LilypondUtils.noteValueMap.get(tempDuration));
		}
		// noteBuf.append(" ");
		// Math.
		return noteBuf.toString();
	}

	public static String getRestValue(Timepoint inTp, double duration) {
		StringBuffer restSb = new StringBuffer("");
		double tempDur = duration;
		if (inTp.getTime() < 1.0 && inTp.getTime() > 0) {
			if (LilypondUtils.restMap.containsKey(1.0 - inTp.getTime())) {
				return LilypondUtils.restMap.get(1.0 - inTp.getTime());
				// return LilypondUtils.restMap.get( "r8 " );
			}
			throw new IllegalArgumentException(String.format(
					"Rest duration value %f is not a legal value.", duration));
			// return "";
		}
		if (duration < 1.0) {
			if (LilypondUtils.restMap.containsKey(duration)) {
				return LilypondUtils.restMap.get(duration);
				// return LilypondUtils.restMap.get( "r8 " );
			}
			throw new IllegalArgumentException(String.format(
					"Rest duration value %f is not a legal value.", duration));
		}
		Double headRest = Math.floor(inTp.getTime()) + 1 - inTp.getTime();
		if (headRest > 0 && LilypondUtils.restMap.containsKey(headRest)) {
			restSb.append(LilypondUtils.restMap.get(headRest));
			tempDur -= headRest;
		}
		while (tempDur >= 1.0) {
			restSb.append(LilypondUtils.restMap.get(1.0));
			tempDur -= 1.0;
		}
		if (tempDur > 0) {
			if (LilypondUtils.restMap.containsKey(tempDur)) {
				restSb.append(LilypondUtils.restMap.get(tempDur));
			}
		}
		return restSb.toString();
	}

	// A special case for rests at the beginning of a counterpoint, i.e. the
	// rest duration equals
	// phrase.getTimepoint().getTime().
	public static String getRestValue(Timepoint inTp) {
		StringBuffer restSb = new StringBuffer("");
		double tempDur = inTp.getTime();
		if (inTp.getTime() < 1.0 && inTp.getTime() > 0.0) {
			if (LilypondUtils.restMap.containsKey(inTp.getTime())) {
				return LilypondUtils.restMap.get(inTp.getTime());
				// return LilypondUtils.restMap.get( "r8 " );
			}
			throw new IllegalArgumentException(String.format(
					"Rest duration value %f is not a legal value.", tempDur));
			// return "";
		} else {
			while (tempDur >= 1.0) {
				if (tempDur >= 4.0) {
					restSb.append(LilypondUtils.restMap.get(4.0));
					tempDur -= 4.0;
				} else if (tempDur >= 2.0 && (int) tempDur % 2 == 0) {
					restSb.append(LilypondUtils.restMap.get(2.0));
					tempDur -= 2.0;
				} else {
					restSb.append(LilypondUtils.restMap.get(1.0));
					tempDur -= 1.0;
				}
			}
			if (tempDur > 0.0) {
				if (LilypondUtils.restMap.containsKey(tempDur)) {
					restSb.append(LilypondUtils.restMap.get(tempDur));
				} else {
					throw new IllegalArgumentException(String.format(
							"Rest duration value %f is not a legal value.",
							tempDur));
				}
			}
		}
		return restSb.toString();
	}
}
