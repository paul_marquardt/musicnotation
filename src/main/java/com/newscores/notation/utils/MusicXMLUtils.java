package com.newscores.notation.utils;

import java.util.*;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MusicXMLUtils {

	// private Phrase phraseToDecorate;
	
	public static Document addNotes( Document doc, List<Integer> notes, int duration, int divisions) {
		int noteIndex = 0;
		for ( int note : notes ) {
			Element noteElement = doc.createElement("note");
			if ( noteIndex > 0 ) {
				noteElement.appendChild( doc.createElement("chord") );
			}
			noteElement.setAttribute("default-x", "80");
			Element pitchElement = doc.createElement("pitch");
			Element stepElement = doc.createElement("step");
			stepElement.appendChild( doc.createTextNode("C"));
			pitchElement.appendChild( stepElement );
			Element octaveElement = doc.createElement("octave");
			octaveElement.appendChild( doc.createTextNode( "3") );
			pitchElement.appendChild( octaveElement );
			noteElement.appendChild( pitchElement );
			
			Element durationElement = doc.createElement("duration");
			durationElement.appendChild( doc.createTextNode( String.format("%d", duration ) ) );
			noteElement.appendChild( durationElement );

			Element voiceElement = doc.createElement("voice");
			voiceElement.appendChild( doc.createTextNode( "1" ) );
			noteElement.appendChild( voiceElement );
			Element typeElement = doc.createElement("type");
			typeElement.appendChild( doc.createTextNode("quarter") );
			noteIndex++;
		}
		return doc;
	}
	
/**
    <note default-x="80">
    <pitch>
      <step>G</step>
      <octave>3</octave>
    </pitch>
    <duration>32</duration>
    <voice>1</voice>
    <type>quarter</type>
  </note>
  <note default-x="80">
    <chord/>
    <pitch>
      <step>B</step>
      <octave>3</octave>
    </pitch>
    <duration>32</duration>
    <voice>1</voice>
    <type>quarter</type>
  </note>
  <note default-x="80">
    <chord/>
    <pitch>
      <step>D</step>
      <octave>4</octave>
    </pitch>
    <duration>32</duration>
    <voice>1</voice>
    <type>quarter</type>
  </note>
**/
	
}
