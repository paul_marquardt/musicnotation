package com.newscores.notation.utils;

public class MusicXMLNote {

	public static String SIXTEENTH = "sixteenth";
	public static String EIGHT = "eight";
	public static String QUARTER = "quarter";
	public static String HALF = "half";
	public static String WHOLE = "whole";
	private int pitch;
	private int duration;
	private int voice;
	private String type;
	
	public MusicXMLNote() {
		new MusicXMLNote( 60 );
	}
	
	public MusicXMLNote( int pitch ) {
		this.setDuration( 32 );
		this.setType( MusicXMLNote.QUARTER );
		this.setVoice( 1 );
		this.setPitch( 60 );
	}
	
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getVoice() {
		return voice;
	}
	public void setVoice(int voice) {
		this.voice = voice;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public int getPitch() {
		return pitch;
	}

	public void setPitch(int pitch) {
		this.pitch = pitch;
	}
}


/**
      <note>
        <pitch>
          <step>F</step>
          <octave>3</octave>
        </pitch>
        <duration>32</duration>
        <voice>1</voice>
        <type>quarter</type>
      </note>
**/