package com.newscores.notation;

import com.newscores.notation.interfaces.ObjectDecoratorInterface;

public class LilypondAbstractCMWriter implements ObjectDecoratorInterface {

	// private Phrase phraseToDecorate;
	public static final String TREBLE_CLEF = "\\clef treble\n";
	public static final String BASS_CLEF = "\\clef bass\n";
	public static final String DEFAULT_CLEF = TREBLE_CLEF;
	public static final String DEFAULT_ACCIDENTAL = "s";
	private StringBuffer outputBuffer;

	public LilypondAbstractCMWriter(ObjectDecoratorInterface inDec, String desc ) {
		this.outputBuffer = new StringBuffer();
		this.writeStaves(inDec, desc);
	}

	public String output() {
		// TODO Auto-generated method stub
		return this.outputBuffer.toString();
	}

	private void writeStaves( ObjectDecoratorInterface objDecorator, String description ) {
		// write the Phrase description as a comment.
		this.outputBuffer.append("\\score {\n");
		this.outputBuffer.append("<<\n");
		this.outputBuffer.append(objDecorator.output());
				// only write the header under the first Staff object.
		this.outputBuffer.append("\n");
		/*
		this.outputBuffer.append( "\\layout {\n" );
		this.outputBuffer.append( "  \\context {\n" );
		this.outputBuffer.append( "	    \\Staff\n" );
		this.outputBuffer.append( "	    \\remove Time_signature_engraver\n" );
		this.outputBuffer.append( "	 }\n" );
		this.outputBuffer.append( "}\n" );
		*/
		this.outputBuffer.append(">>\n");
		this.outputBuffer.append("\t\\header {\n");
		this.outputBuffer.append(String.format("\t\tpiece = \"%s\"\n", description ) );
		this.outputBuffer.append("\t}\n");
		this.outputBuffer.append("}\n\n");
	}

}
