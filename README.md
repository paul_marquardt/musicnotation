# README #

The mission of this project is to create a Java library that enables the rendering of musical ideas in LilyPond notation format. Its target audience is composers who would like to automate the notation of their music. The library aims to provide a set of high-level musical objects, such as Phrase, Chord, Part, etc, which can be rendered to musical notation.

### What is this repository for? ###

* This repository is for developers who are interested in contributing to the LilyPond Music Notation project. You do not need the code in this repository to use the library.

### How do I get set up? ###

* The Music Notation project uses Maven as a build tool.
* I try to keep dependencies at a minimum.

### How do I use these libraries? ###

* You do not need the Java code to use the library. In your build tool (Gradle, Maven, Leningen, etc), just add the depdendency for com.newscores.musicnotation.
* Browse the [ Maven repository](https://search.maven.org/) for the latest version. Javadocs are available [here](http://newscores-public-web.s3-website-us-east-1.amazonaws.com/overview-summary.html).
* A build.gradle and sample Groovy scripts are included in src/main/groovy.

### Contribution guidelines ###

* All new functionality must be accompanied by unit tests.

### Who do I talk to? ###

* Paul Marquardt (marqrdt at gmail)

### License Information ###
* Copyright [2016] [Paul Marquardt]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

  